'use strict'

import util from '../src/index'
import { assert } from 'chai'
import nodeassert from 'assert'

describe('util Array methods', () => {
  describe('util.arrayContainsString', () => {
    it('should detect strings', () => {
      assert.isTrue(util.arrayContainsString(['r1', 't2', 'e3'], 'r1'), 'can detect strings')
      assert.isFalse(util.arrayContainsString(['r1', 't2', 'e3'], 'r11'), 'can detect strings')
    })
  })
  describe('util.arrayContainsString with parameter loose', () => {
    it('should detect strings', () => {
      assert.isTrue(util.arrayContainsString(['r1', 't2', 'e3'], 'r', true), 'can detect substrings')
      assert.isTrue(util.arrayContainsString(['r1', 't2', 'e3'], '3', true), 'can detect substrings')
    })
  }) 
})
describe('util Object methods', () => {
  describe('util.isPlainObject', () => {
    it('should accurately detect objects', () => {
      nodeassert.strictEqual(true, util.isPlainObject({}), '{} is a plain Object')
      nodeassert.strictEqual(true, util.isPlainObject({one:1, two:2}), '{one:1, two:2} is a plain Object')
      nodeassert.strictEqual(true, util.isPlainObject(new Object()), 'new Object() is a plain Object')
      nodeassert.strictEqual(true, util.isPlainObject(new RegExp()), 'new RegExp() is a plain Object')
      nodeassert.strictEqual(true, util.isPlainObject((function(){
        return arguments
      })('one', 2)), 'arguments is a plain Object')
      nodeassert.notStrictEqual(true, util.isPlainObject(null), 'null is NOT a plain Object')
      nodeassert.notStrictEqual(true, util.isPlainObject([]), '[] is NOT a plain Object')
      nodeassert.notStrictEqual(true, util.isPlainObject([1, 2, 3]), '[1, 2, 3] is NOT a plain Object')
      nodeassert.notStrictEqual(true, util.isPlainObject(4), '4 is NOT a plain Object')
      nodeassert.notStrictEqual(true, util.isPlainObject('4'), '\'4\' is NOT a plain Object')
      nodeassert.notStrictEqual(true, util.isPlainObject(undefined), 'undefined is NOT a plain Object')
    })
  })
})
describe('prototype methods', () => {
  it('should fail before util.usePrototypeExtensions() is called', () => {
    assert.notProperty(Array.prototype, 'containsString', 'containsString doesn\'t exist on Array prototype')
    assert.notProperty(Object.prototype, 'isPlainObject', 'isPlainObject doesn\'t exist on Object prototype')
  })
  it('should add the prototype methods after util.usePrototypeExtensions() is called', () => {
    util.usePrototypeExtensions()
    assert.property(Array.prototype, 'containsString', 'containsString exists on Array prototype')
    assert.property(Object.prototype, 'isPlainObject', 'isPlainObject exists on Object prototype')
  })
  describe('Array.prototype extensions', () => {
    describe('Array.prototype.containsString', () => {
      it('should detect strings', () => {
        assert.isTrue(['r1', 't2', 'e3'].containsString('r1'), 'can detect strings')
        assert.isFalse(['r1', 't2', 'e3'].containsString('r11'), 'can detect strings')
      })
    })
    describe('Array.prototype.containsString with parameter loose', () => {
      it('should detect substrings', () => {
        assert.isTrue(['r1', 't2', 'e3'].containsString('r', true), 'can detect substrings')
        assert.isTrue(['r1', 't2', 'e3'].containsString('3', true), 'can detect substrings')
      })
    }) 
  })
  describe('Object.prototype extensions', () => {
    describe('Object.prototype.isPlainObject', () => {
      it('should accurately detect objects', () => {
        nodeassert.strictEqual(true, {}.isPlainObject(), '{} is a plain Object')
        nodeassert.throws(() => {null.isPlainObject()}, TypeError, 'null is NOT a plain Object')
        nodeassert.strictEqual(true, {one:1, two:2}.isPlainObject(), '{one:1, two:2} is a plain Object')
        nodeassert.strictEqual(true, new Object().isPlainObject(), 'new Object() is a plain Object')
        nodeassert.strictEqual(true, new RegExp().isPlainObject(), 'new RegExp() is a plain Object')
        nodeassert.strictEqual(true, (function(){
          return arguments
        })('one', 2).isPlainObject(), 'arguments is a plain Object')
        nodeassert.notStrictEqual(true, [].isPlainObject(), '[] is NOT a plain Object')
        nodeassert.notStrictEqual(true, [1, 2, 3].isPlainObject(), '[1, 2, 3] is NOT a plain Object')
        nodeassert.notStrictEqual(true, (4).isPlainObject(), '4 is NOT a plain Object')
        nodeassert.notStrictEqual(true, '4'.isPlainObject(), '\'4\' is NOT a plain Object')
        nodeassert.throws(() => {undefined.isPlainObject()}, TypeError, 'undefined is NOT a plain Object')
      })
    })
  })
})
