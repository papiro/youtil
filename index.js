'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _typeof(obj) { return obj && typeof Symbol !== "undefined" && obj.constructor === Symbol ? "symbol" : typeof obj; }

var youtil = {
  usePrototypeExtensions: function usePrototypeExtensions() {
    var prototypeExtensions = {
      Object: (function (_Object) {
        function Object() {
          return _Object.apply(this, arguments);
        }

        Object.toString = function () {
          return _Object.toString();
        };

        return Object;
      })(function () {
        if (!Object.prototype.hasOwnProperty('isPlainObject')) {
          Object.prototype.isPlainObject = function () {
            return youtil.isPlainObject(this);
          };
        }
      }),
      Array: (function (_Array) {
        function Array() {
          return _Array.apply(this, arguments);
        }

        Array.toString = function () {
          return _Array.toString();
        };

        return Array;
      })(function () {
        if (!Array.prototype.hasOwnProperty('containsString')) {
          Array.prototype.containsString = function (needle, loose) {
            return youtil.arrayContainsString(this, needle, loose);
          };
        }
      })
    };
    for (var _len = arguments.length, primitives = Array(_len), _key = 0; _key < _len; _key++) {
      primitives[_key] = arguments[_key];
    }

    (primitives.length ? primitives : Object.keys(prototypeExtensions))['forEach'](function (primitive) {
      prototypeExtensions[primitive]();
    });
  },
  arrayContainsString: function arrayContainsString(haystack, needle, loose) {
    var sep = '_-_';
    return loose ? RegExp(needle).test(haystack.join(sep)) : RegExp('^' + needle + sep + '|' + sep + needle + sep + '|' + sep + needle).test(haystack.join(sep));
  },
  isPlainObject: function isPlainObject(obj) {
    return (typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) === 'object' && !Array.isArray(obj) && obj !== null;
  }
};

exports.default = youtil;
