'use strict'

let youtil = {
  usePrototypeExtensions(...primitives) {
    let prototypeExtensions = {
      Object() {
        if( !Object.prototype.hasOwnProperty('isPlainObject') ) {
          Object.prototype.isPlainObject = function() {
            return youtil.isPlainObject(this)
          }
        }
      },
      Array() {
        if( !Array.prototype.hasOwnProperty('containsString') ) {
          Array.prototype.containsString = function(needle, loose) {
            return youtil.arrayContainsString(this, needle, loose)
          }
        }
      }
    }
    ;(primitives.length ? primitives : Object.keys(prototypeExtensions))['forEach'](primitive => {
      prototypeExtensions[primitive]()
    })
  },
  arrayContainsString(haystack, needle, loose) {
    const sep = '_-_'
    return loose ?
      RegExp(needle).test(haystack.join(sep)) :
      RegExp(`^${needle}${sep}|${sep}${needle}${sep}|${sep}${needle}`).test(haystack.join(sep))
  },
  isPlainObject(obj) {
    return typeof obj === 'object' && !Array.isArray(obj) && obj !== null
  }
}

export default youtil
